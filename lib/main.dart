import 'package:app_template/src/app.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final appDocDir = await getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);
  await Hive.openBox(Constants.BOX_NAME);
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    systemNavigationBarColor: Constants.kitGradients[0], // navigation bar color
    statusBarColor: Constants.kitGradients[2], // status bar color
  ));

  runApp(MyApp());
}
