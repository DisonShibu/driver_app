import 'package:app_template/localisation/demo_local.dart';
import 'package:flutter/cupertino.dart';

String getTranslated(BuildContext context, String key) {
  return DemoLocalizations.of(context).getTranslatedValues(key);
}
