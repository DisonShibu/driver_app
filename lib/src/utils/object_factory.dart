import 'hive.dart';

/// it is a hub that connecting pref,repo,client
/// used to reduce imports in pages
class ObjectFactory {
  static final _objectFactory = ObjectFactory._internal();

  ObjectFactory._internal();

  factory ObjectFactory() => _objectFactory;

  ///Initialisation of Objects
  AppHive _appHive = AppHive();

  ///
  /// Getters of Objects
  ///

  AppHive get appHive => _appHive;

  ///
  /// Setters of Objects
  ///

}
