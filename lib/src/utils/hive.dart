import 'package:hive/hive.dart';
import 'constants.dart';

class AppHive {
  // void main (){}

  // void openBox()async{
  //   await Hive.openBox(Constants.BOX_NAME);
  //
  // }
  static const String _USER_ID = "user_id";
  static const String _NAME = "name";
  static const String _TOKEN = "token";
  static const String _EMAIL = "email";
  static const String _PREFERENCE = "preferenceid";
  static const String _LATITUDE = "longitude";
  static const String _XUSER = "newuserid";
  static const String _PASSWORD = "userpassword";
  static const String _LANGUAGE = "language";
  static const String _Address = "Address";
  static const String _LandMark = "LandMark";
  static const String _HOUSENO = "Houseno";

  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGet({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  putUserId({String userId}) {
    hivePut(key: _USER_ID, value: userId);
  }

  String getUserId() {
    return hiveGet(key: _USER_ID);
  }

  putUserPassword({String email}) {
    hivePut(key: _PASSWORD, value: email);
  }

  String getUserPassword() {
    return hiveGet(key: _PASSWORD);
  }

  putName({String name}) {
    hivePut(key: _NAME, value: name);
  }

  String getName() {
    return hiveGet(key: _NAME);
  }

  putToken({String token}) {
    hivePut(key: _TOKEN, value: token);
  }

  putLanguage({String locale}) {
    hivePut(key: _TOKEN, value: locale);
  }

  String getLanguage() {
    return hiveGet(key: _TOKEN);
  }

  String getToken() {
    return hiveGet(key: _TOKEN);
  }

  String getEmail() {
    return hiveGet(key: _EMAIL);
  }

  putEmail(String value) {
    return hivePut(key: _EMAIL, value: value);
  }

  String getAddress() {
    return hiveGet(key: _Address);
  }

  putAddress(String value) {
    return hivePut(key: _Address, value: value);
  }

  String getHouseNo() {
    return hiveGet(key: _HOUSENO);
  }

  putHouseNO(String value) {
    return hivePut(key: _HOUSENO, value: value);
  }

  String getLandMark() {
    return hiveGet(key: _LandMark);
  }

  putLandMark(String value) {
    return hivePut(key: _LandMark, value: value);
  }

  AppHive();
}
