import 'dart:io';
import 'package:app_template/src/screens/upload_files_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/login_text_box.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/material.dart';
import 'package:email_validator/email_validator.dart';

class RegistrationPage extends StatefulWidget {
  @override
  _RegistrationPageState createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController phoneNumberTextEditingController =
      new TextEditingController();

  bool isValid;

  bool validatEmail(String email) {
    isValid = EmailValidator.validate(email);

    return isValid;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
        child: AppBarDelivery(
          title: "RegistrationPage",
          elevation: 3,
          onPressedLeftIcon: () {
            pop(context);
          },
          color: Constants.kitGradients[0],
          leftIcon: Icons.arrow_back_ios,
          leftIconTrue: false,
          isWhite: true,
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 10)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 10),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 2.6),
                  height: screenHeight(context, dividedBy: 5),
                  child: Image.asset(
                    "assets/images/delivery_app_icon.jpg",
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Row(
                  children: [
                    Container(
                        width: screenWidth(context, dividedBy: 1.7),
                        child: FittedBox(
                            child: Text(
                          "Welcome to flutter resturant !",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 30,
                              fontFamily: 'PromptLight'),
                        ))),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LoginTextBox(
                        controller: emailTextEditingController,
                        icon: Icons.email_outlined,
                        hintText: "Email",
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      LoginTextBox(
                        phoneNumber: true,
                        controller: nameTextEditingController,
                        hintText: "PhoneNumber",
                        password: false,
                        icon: Icons.phone_android,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),
                      LoginTextBox(
                        controller: phoneNumberTextEditingController,
                        icon: Icons.person,
                        hintText: "Name",
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                RestaurantButton(
                  onPressed: () {
                    if (nameTextEditingController.text != "" &&
                        phoneNumberTextEditingController.text != "" &&
                        emailTextEditingController.text != "") {
                      if (validatEmail(emailTextEditingController.text) ==
                          true) {
                        push(context, UploadFilesPage());
                      } else {
                        showToast("Enter a valid email");
                      }
                    } else {
                      showToast("Please enter the valid details");
                    }
                  },
                  title: "NEXT",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
