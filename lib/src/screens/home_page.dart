import 'dart:async';
import 'dart:io';
import 'package:app_template/src/screens/completed_orders_page.dart';
import 'package:app_template/src/screens/drawer.dart';
import 'package:app_template/src/screens/new_orders_page.dart';
import 'package:app_template/src/screens/notification_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/category_widget.dart';
import 'package:background_location/background_location.dart';
import 'package:flutter/material.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:geolocator/geolocator.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool boyOnline = false;
  List<String> gridTitle = ["New Orders", "Pending Orders"];
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();

  StreamSubscription<Position> positionStream;
  Stream<Location> locationStream;
  String newLocation = "";

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DoubleBack(
      message: "Press back again to quit",
      child: Scaffold(
        key: _globalKey,
        drawer: DrawerWidget(),
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "Home Page",
            elevation: 0,
            onPressedLeftIcon: () {
              _globalKey.currentState.openDrawer();
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.menu,
            leftIconTrue: true,
            centreTitle: true,
            rightIconTrue: true,
            rightIcon: Icons.notifications,
            onPressedRightIcon: () {
              push(context, NotificationPage());
            },
            isWhite: true,
          ),
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 15)),
          child: Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 20),
              ),
              Row(
                children: [
                  Container(
                      height: screenHeight(context, dividedBy: 15),
                      width: screenWidth(context, dividedBy: 2.8),
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[2],
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            boyOnline == true ? "Online" : "Offline",
                            style: TextStyle(
                                color: Constants.kitGradients[5],
                                fontFamily: "OswaldRegualar",
                                fontSize: 20),
                          ),
                          Switch(
                            value: boyOnline,
                            activeColor: Constants.kitGradients[6],
                            inactiveThumbColor: Constants.kitGradients[12],
                            onChanged: (isOn) async {
                              setState(() {
                                boyOnline = isOn;
                              });
                              if (boyOnline == true) {
                                _getUseLocationUpdates();
                                // locationStream.listen((event) {
                                //   showToast(event.latitude.toString());
                                //   print(event.toString());
                                // });
                              } else {
                                await BackgroundLocation.stopLocationService();
                              }
                            },
                          ),
                        ],
                      )),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 10),
              ),
              Row(
                children: [
                  Text(
                    boyOnline == true
                        ? "Rahul is ready to deliver"
                        : "Rahul is busy",
                    style: TextStyle(
                        color: Constants.kitGradients[5],
                        fontFamily: "PromPlight",
                        fontSize: 20),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 10),
              ),
              GridView.builder(
                itemCount: 2,
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                  childAspectRatio: 1,
                ),
                itemBuilder: (
                  context,
                  index,
                ) {
                  print(gridTitle[index].toString());

                  return CategoryWidget(
                    onPressed: () {
                      if (index == 0) {
                        push(context, NewOrdersPage());
                      } else {
                        push(context, CompletedOrdersPage());
                      }
                    },
                    title: gridTitle[index],
                    images: "assets/images/food_del.jpg",
                  );
                },
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 10),
              ),
              Center(
                child: Text(
                  "Be Ready To Deliver",
                  style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontFamily: "PromPlight",
                      fontSize: 18),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<bool> checkPermissions() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (serviceEnabled == true) {
      return serviceEnabled;
    } else {
      showToast("Please Enable Your Location Service");

      Future.error("Location Service Disabled");
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.whileInUse ||
        permission == LocationPermission.always) {
      return true;
    } else if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error("");
      }
    } else if (permission == LocationPermission.deniedForever) {
      if (Platform.isAndroid) {
        await Geolocator.openLocationSettings();
      } else {
        print("Ios");
      }
      return Future.error("Location Service Are Denied");
      // Permissions are denied forever, handle appropriately.

    } else {
      return true;
    }
  }

  void _getUseLocationUpdates() async {
    print("GET USER METHOD RUNNING =========");
    checkPermissions().then((bool) async {
      print("Location Fetched");
      // positionStream =
      //     Geolocator.getPositionStream(intervalDuration: Duration(seconds: 3))
      //         .listen((Position position) {
      //   print(position == null
      //       ? 'Unknown'
      //       : "loc" +
      //           position.latitude.toString() +
      //           ', ' +
      //           "loc" +
      //           position.longitude.toString());
      // });
      await BackgroundLocation.setAndroidNotification(
        title: 'Background service is running',
        message: 'Background location in progress',
        icon: '@mipmap/ic_launcher',
      );
      //await BackgroundLocation.setAndroidConfiguration(1000);
      await BackgroundLocation.startLocationService(distanceFilter: 50);

      BackgroundLocation.getLocationUpdates((location) {
        setState(() {
          newLocation = location.latitude.toString();
        });
        print(location.latitude);
      });
    }).onError((error, stackTrace) {
      print("Error" + error.toString());
    });
  }
}
