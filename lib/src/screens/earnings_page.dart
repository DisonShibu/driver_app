import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/delivery_distance_widget.dart';
import 'package:app_template/src/widgets/file_upload_text.dart';
import 'package:app_template/src/widgets/tab_bar_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EarningsPage extends StatefulWidget {
  @override
  _EarningsPageState createState() => _EarningsPageState();
}

class _EarningsPageState extends State<EarningsPage> {
  int currentIndex = 0;
  DateTime selectedDate;
  String formattedDate;
  DateTime formattedDateTime;
  DateTime date;
  _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(), // Refer step 1
      firstDate: new DateTime(
        date.year,
        date.month - 1,
      ),
      lastDate: new DateTime.now(),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        formattedDate = DateFormat('dd/MM/yyyy').format(selectedDate);
        formattedDateTime =
            DateTime.parse(DateFormat('yyyy-MM-dd').format(selectedDate));
        print(selectedDate);
      });
  }

  @override
  void initState() {
    date = DateTime.now();
    selectedDate = DateTime.now();
    formattedDate = DateFormat('dd/MM/yyyy').format(selectedDate);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
        child: AppBarDelivery(
          title: "Earnings",
          elevation: 0,
          onPressedLeftIcon: () {
            pop(context);
          },
          color: Constants.kitGradients[0],
          leftIcon: Icons.arrow_back_ios,
          leftIconTrue: true,
          centreTitle: false,
          rightIconTrue: false,
          onPressedRightIcon: () {},
          isWhite: true,
        ),
      ),
      body: Scaffold(
          body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 30)),
        child: Column(
          children: [
            Card(
              margin: EdgeInsets.zero,
              child: Container(
                height: screenHeight(context, dividedBy: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TabBarTile(
                      title: "Monthly",
                      tileSelected: currentIndex == 0 ? true : false,
                      onPressed: () {
                        currentIndex = 0;
                        setState(() {});
                      },
                    ),
                    TabBarTile(
                      title: "Trips",
                      tileSelected: currentIndex == 1 ? true : false,
                      onPressed: () {
                        setState(() {
                          currentIndex = 1;
                        });
                      },
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                _selectDate(context);
              },
              child: Row(
                children: [
                  FileUploadWidget(
                    width: 1.3,
                    icon: Icons.calendar_today,
                    imageName: formattedDate,
                    onPressed: () {
                      _selectDate(context);
                    },
                  ),
                ],
              ),
            ),
            Container(
                decoration: BoxDecoration(
                    color: Constants.kitGradients[0],
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.grey)),
                child: currentIndex == 0
                    ? Column(
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          DeliveryDistanceWidget(
                            title: "Earnings this Month",
                            subHeading: "Your Earnings",
                            orderNumber: false,
                            doubleHeight: 10,
                            price: "3588888",
                            doubleWidth: 1.5,
                            thirdLine: "Your Earnings",
                          ),
                          Divider(
                            height: 10,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          DeliveryDistanceWidget(
                            iconBool: true,
                            icon: Icons.monetization_on_outlined,
                            title: "Incentives",
                            doubleHeight: 10,
                            orderNumber: false,
                            price: "3588888",
                            doubleWidth: 2,
                            thirdLine: "This Month",
                          ),
                          Divider(
                            height: 10,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 40),
                          ),
                          DeliveryDistanceWidget(
                            iconBool: true,
                            icon: Icons.calculate_outlined,
                            title: "Total",
                            doubleHeight: 10,
                            orderNumber: false,
                            price: "3588888",
                            doubleWidth: 2,
                            thirdLine: "This Month",
                          ),
                        ],
                      )
                    : ListView.builder(
                        shrinkWrap: true,
                        itemCount: 3,
                        physics: AlwaysScrollableScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 40),
                              ),
                              DeliveryDistanceWidget(
                                  title: "Imperial Restaurant",
                                  orderNumber: true,
                                  iconBool: false,
                                  thirdLine: "3.4 km",
                                  subHeading: "12:34",
                                  doubleWidth: 1.6,
                                  doubleHeight: 8,
                                  price: "34343434"),
                              index == 2
                                  ? DeliveryDistanceWidget(
                                      iconBool: true,
                                      icon: Icons.calculate_outlined,
                                      title: "Total Distance ",
                                      doubleHeight: 12,
                                      orderNumber: false,
                                      price: "200",
                                      doubleWidth: 2,
                                      thirdLine: "50 km",
                                    )
                                  : Container()
                            ],
                          );
                        }))
          ],
        ),
      )),
    );
  }
}
