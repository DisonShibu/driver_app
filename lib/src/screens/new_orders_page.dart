import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/new_order_details_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/delivery_distance_widget.dart';
import 'package:app_template/src/widgets/list_tile_neworders.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NewOrdersPage extends StatefulWidget {
  @override
  _NewOrdersPageState createState() => _NewOrdersPageState();
}

class _NewOrdersPageState extends State<NewOrdersPage> {
  Future<bool> onWillPop() {
    pushAndRemoveUntil(context, HomePage(), false);
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 16)),
          child: AppBarDelivery(
            title: "",
            elevation: 0,
            onPressedLeftIcon: () {
              pushAndRemoveUntil(context, HomePage(), false);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          ),
        ),
        body: Container(
          height: screenHeight(context, dividedBy: 1),
          child: SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                    height: screenHeight(context, dividedBy: 4),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[2],
                        borderRadius: BorderRadius.circular(20)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        Container(
                          child: Center(
                            child: Text(
                              "New Orders",
                              style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontFamily: "PrompLight",
                                  fontSize:
                                      screenWidth(context, dividedBy: 20)),
                            ),
                          ),
                          width: screenWidth(context, dividedBy: 1),
                        ),
                      ],
                    )),
                Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 7),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Constants.kitGradients[0],
                          borderRadius: BorderRadius.circular(20)),
                      child: ListView.builder(
                          shrinkWrap: true,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: 3,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical:
                                      screenHeight(context, dividedBy: 40)),
                              child: ListTileNewOrders(
                                userName: "Jhone Doe",
                                userAddress:
                                    "Near Seetha devi Temple chakkarparamaba, Near piplleinJunction, Edpallly Kochi",
                                hotelAddress:
                                    "near Seetha devi Temple chakkarparamaba, Near piplleinJunction, Edpallly Kochi",
                                hotelName: "Kabani",
                                elevation: 1,
                                onPressedViewDetails: () {
                                  push(context, OrderDetailsPage());
                                },
                                buttonTrue: true,
                              ),
                            );
                          }),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
