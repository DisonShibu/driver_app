import 'package:app_template/src/app.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/object_factory.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/build_dropdown_categories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  String language = "";
  String code = "";

  List<String> languageList = [
    "Arabic ",
    "English",
  ];

  bool notifications;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AppBarDelivery(
              isWhite: false,
              color: Constants.kitGradients[0],
              title: "Settings",
              leftIcon: Icons.arrow_back_ios,
              onPressedLeftIcon: () {
                pop(context);
              },
            ),
          ],
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 15)),
        child: Column(
          children: [
            Row(
              children: [
                AddressHeadingWidget(
                  title: "Language",
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),
            DropDownFormField(
              hintText: true,
              dropDownList: languageList,
              fontFamily: "OpenSansRegular",
              textBoxWidth: 1.36,
              boxHeight: 12,
              boxWidth: 1.2,
              dropDownValue: language,
              hintFontFamily: "OpenSansRegular",
              underline: true,
              onClicked: (value) {
                if (value == "English") {
                  language = value;
                  code = "en";
                } else {
                  language = value;
                  code = "ar";
                }

                ObjectFactory().appHive.putLanguage(locale: language);
                print(ObjectFactory().appHive.getLanguage().toString());

                setState(() {
                  language = value;
                });
                MyApp.setLocale(context, Locale(code));
              },
              hintPadding: screenWidth(context, dividedBy: 3),
              title: "Langauge",
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: AddressHeadingWidget(title: "Notifications"),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Container(
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 100),
                        ),
                        Switch(
                          value: notifications,
                          activeColor: Constants.kitGradients[6],
                          inactiveThumbColor: Constants.kitGradients[0],
                          onChanged: (isOn) {
                            setState(() {
                              notifications = isOn;
                            });

                            print(notifications);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
