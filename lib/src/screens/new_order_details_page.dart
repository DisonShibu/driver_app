import 'dart:io';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/inlineMapView.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/label_value_widget.dart';
import 'package:app_template/src/widgets/list_tile_food_count.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:app_template/src/widgets/saved_address_widget.dart';
import 'package:flutter/material.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:flutter_mapbox_navigation/library.dart';

class OrderDetailsPage extends StatefulWidget {
  @override
  _OrderDetailsPageState createState() => _OrderDetailsPageState();
}

class _OrderDetailsPageState extends State<OrderDetailsPage> {
  MapBoxNavigation _directions;
  MapBoxOptions _options;
  String _instruction = "";

  bool _isMultipleStop = false;
  MapBoxNavigationViewController _controller;
  double _distanceRemaining, _durationRemaining;
  bool _routeBuilt = false;
  bool _isNavigating = false;
  final destination =
      WayPoint(name: "Way Point 1", latitude: 11.61056, longitude: 76.08222);
  final start =
      WayPoint(name: "Way Point 2", latitude: 11.6854, longitude: 76.1320);

  @override
  void initState() {
    initialize();
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[0],
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
          child: AppBarDelivery(
            title: "New Orders",
            elevation: 0,
            onPressedLeftIcon: () {
              pushAndRemoveUntil(context, HomePage(), false);
            },
            color: Constants.kitGradients[0],
            leftIcon: Icons.arrow_back_ios,
            leftIconTrue: true,
            isWhite: true,
          ),
        ),
        body: ListView(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Row(
                    children: [
                      Text(
                        "Store Address",
                        style: TextStyle(
                            color: Constants.kitGradients[5],
                            fontFamily: "PrompLight",
                            fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    child: SavedAddressBox(
                      center: false,
                      svgIconTrue: false,
                      icon: Icons.location_on_outlined,
                      heading: "Kabani",
                      subHeading:
                          "Near Seetha devi Temple chakkarparamaba, Near piplleinJunction",
                      thirdLineTrue: false,
                      onPressedAddress: () async {},
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 60),
                  ),
                  Row(
                    children: [
                      Text(
                        "Delivery Address",
                        style: TextStyle(
                            color: Constants.kitGradients[5],
                            fontFamily: "PrompLight",
                            fontSize: 16),
                      ),
                    ],
                  ),
                  Container(
                    child: SavedAddressBox(
                      center: false,
                      svgIconTrue: false,
                      icon: Icons.location_on_outlined,
                      image: "assets/images/userprofile.png",
                      heading: "Dison Shibu",
                      subHeading:
                          "Near Seetha devi Temple chakkarparamaba, Near piplleinJunction\n"
                          "LandMark: Temple,",
                      thirdLine: "Ph:7589809808",
                      thirdLineTrue: true,
                      onPressedAddress: () async {
                        var wayPoints = <WayPoint>[];
                        wayPoints.add(start);
                        wayPoints.add(destination);

                        await _directions.startNavigation(
                            wayPoints: wayPoints,
                            options: MapBoxOptions(
                                mode: MapBoxNavigationMode.drivingWithTraffic,
                                simulateRoute: false,
                                language: "en",
                                units: VoiceUnits.metric));
                      },
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Row(
                    children: [
                      AddressHeadingWidget(
                        title: "Order Details",
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Row(
                    children: [
                      Text(
                        "OrderCode :Ord645",
                        style: TextStyle(
                            color: Constants.kitGradients[5],
                            fontFamily: "PrompLight",
                            fontSize: 16),
                      ),
                    ],
                  ),
                  ListView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: 2,
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (BuildContext context, int index) {
                        return ListTileFoodCount(
                          foodcount: "1",
                          foodname: "pizza",
                        );
                      }),
                  Card(
                    elevation: 3,
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 80)),
                      child: LabelAndValueWidget(
                        label: "Payment Method",
                        value: "COD",
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  Card(
                    margin: EdgeInsets.zero,
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: Constants.kitGradients[0])),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: screenWidth(context, dividedBy: 80)),
                      child: Column(
                        children: [
                          SizedBox(
                            height: screenHeight(context, dividedBy: 80),
                          ),
                          LabelAndValueWidget(
                            label: "Delivery Cost",
                            value: "650" + " SR",
                          ),
                          LabelAndValueWidget(
                            label: "Food Cost",
                            value: "650" + " SR",
                          ),
                          LabelAndValueWidget(
                            label: "Total Cost",
                            value: "650" + " SR",
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  RestaurantButton(
                    title: "Confirm",
                    onPressed: () {},
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  RestaurantButton(
                    title: "Picked up Food",
                    disabled: true,
                    onPressed: () {},
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  RestaurantButton(
                    title: "Delivered",
                    disabled: true,
                    onPressed: () {},
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                ],
              ),
            )
          ],
        ));
  }

  Future<void> _onEmbeddedRouteEvent(e) async {
    _distanceRemaining = await _directions.distanceRemaining;
    _durationRemaining = await _directions.durationRemaining;

    switch (e.eventType) {
      case MapBoxEvent.progress_change:
        var progressEvent = e.data as RouteProgressEvent;
        if (progressEvent.currentStepInstruction != null)
          _instruction = progressEvent.currentStepInstruction;
        break;
      case MapBoxEvent.route_building:
      case MapBoxEvent.route_built:
        setState(() {
          _routeBuilt = true;
        });
        break;
      case MapBoxEvent.route_build_failed:
        setState(() {
          _routeBuilt = false;
        });
        break;
      case MapBoxEvent.navigation_running:
        setState(() {
          _isNavigating = true;
        });
        break;
      case MapBoxEvent.on_arrival:
        if (!_isMultipleStop) {
          await Future.delayed(Duration(seconds: 3));
          await _controller.finishNavigation();
        } else {}
        break;
      case MapBoxEvent.navigation_finished:
      case MapBoxEvent.navigation_cancelled:
        setState(() {
          _routeBuilt = false;
          _isNavigating = false;
        });
        break;
      default:
        break;
    }
    setState(() {});
  }

  Future<Position> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (serviceEnabled != true) {
      showToast("Please Enable Your Location Service");

      Future.error("Location Service Disabled");
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
    }
    if (permission == LocationPermission.deniedForever) {
      if (Platform.isAndroid) {
        await Geolocator.openLocationSettings();
      } else {
        print("Ios");
      }
      return Future.error("Location Service Are Denied");
      // Permissions are denied forever, handle appropriately.

    }

    return await Geolocator.getCurrentPosition();
  }

  void _getUserAddress() async {
    print("GET USER METHOD RUNNING =========");
    _determinePosition().then((position) async {
      print("Location Fetched");

      start.latitude = position.latitude;
      start.longitude = position.longitude;
      print(position);
      print(start.toString());
    }).onError((error, stackTrace) {
      print("Error" + error.toString());
    });
  }

  Future<void> initialize() async {
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;
    // await _getUserAddress();

    _directions = MapBoxNavigation(onRouteEvent: _onEmbeddedRouteEvent);
    _options = MapBoxOptions(
        //initialLatitude: 36.1175275,
        //initialLongitude: -115.1839524,

        zoom: 15.0,
        tilt: 0.0,
        bearing: 0.0,
        enableRefresh: false,
        alternatives: true,
        voiceInstructionsEnabled: true,
        bannerInstructionsEnabled: true,
        allowsUTurnAtWayPoints: true,
        mode: MapBoxNavigationMode.drivingWithTraffic,
        units: VoiceUnits.imperial,
        simulateRoute: false,
        animateBuildRoute: true,
        longPressDestinationEnabled: true,
        language: "en");

    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      platformVersion = await _directions.platformVersion;
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    setState(() {});
  }
}
