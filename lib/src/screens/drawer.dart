import 'package:app_template/src/screens/earnings_page.dart';
import 'package:app_template/src/screens/profile_page.dart';
import 'package:app_template/src/screens/settings_page.dart';
import 'package:app_template/src/screens/update_driver%20details_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/urls.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/profile_icon_widget.dart';
import 'package:app_template/src/widgets/profile_page_information_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: Container(
          color: Constants.kitGradients[0],
          width: screenWidth(context, dividedBy: 1.6),
          height: screenHeight(context, dividedBy: 1),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: screenHeight(context, dividedBy: 3.2),
                decoration: BoxDecoration(
                    color: Constants.kitGradients[2],
                    gradient: LinearGradient(
                        colors: [
                          Constants.kitGradients[2],
                          Constants.kitGradients[0]
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter)),
                child: ProfileIconWidget(
                  color: Constants.kitGradients[0],
                  phoneNumber: "7560886107",
                  imageName:
                      "https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-profile-line-black-icon-png-image_691065.jpg",
                  userName: "Dison Shibu",
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 40)),
                child: Container(
                  height: screenHeight(context, dividedBy: 1.9),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      ProfilePageInformationTile(
                        icon: Icons.person_outline,
                        title: " Manage Profile",
                        onPressed: () {
                          push(context, ProfilePage());
                        },
                      ),
                      Spacer(
                        flex: 1,
                      ),

                      ProfilePageInformationTile(
                        icon: Icons.money,
                        title: "Earnings",
                        onPressed: () {
                          push(context, EarningsPage());
                        },
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      ProfilePageInformationTile(
                        icon: Icons.phone_android,
                        title: "Settings",
                        onPressed: () {
                          push(context, SettingsPage());
                        },
                      ),
                      Spacer(
                        flex: 1,
                      ),

                      Center(
                        child: ProfilePageInformationTile(
                          icon: Icons.logout,
                          title: "LogOut",
                          onPressed: () {},
                        ),
                      ),
                      Spacer(
                        flex: 1,
                      ),

                      // ProfilePageInformationTile(
                      //   icon: "assets/images/logout.png",
                      //   title: "Logout",
                      //   onPressed: () {},
                      // ),
                      // Spacer(
                      //   flex: 1,
                      // ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
