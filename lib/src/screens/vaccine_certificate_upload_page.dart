import 'dart:io';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_box_text_feild.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/file_upload_text.dart';
import 'package:app_template/src/widgets/login_text_box.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class VaccineInformationUpload extends StatefulWidget {
  @override
  _VaccineInformationUploadState createState() =>
      _VaccineInformationUploadState();
}

class _VaccineInformationUploadState extends State<VaccineInformationUpload> {
  TextEditingController tempTextEditingController;
  File vaccineCertificate;

  Future<void> pickVaccineCertificate() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'pdf', 'doc'],
    );

    if (result != null) {
      File fileName = File(result.files.single.path);
      vaccineCertificate = fileName;
      setState(() {});
    } else {
      // User canceled the picker
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
        child: AppBarDelivery(
          title: "Vaccine Details",
          elevation: 0,
          onPressedLeftIcon: () {
            pop(context);
          },
          color: Constants.kitGradients[0],
          leftIcon: Icons.arrow_back_ios,
          leftIconTrue: true,
          isWhite: true,
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 10)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Row(
                  children: [
                    Container(
                        width: screenWidth(context, dividedBy: 1.7),
                        child: Text(
                          "Vaccine Details ",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 25,
                              fontFamily: 'OswaldRegular'),
                        )),
                  ],
                ),
                Row(
                  children: [
                    AddressHeadingWidget(
                      title: "Upload Certificate",
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Card(
                  elevation: 3,
                  margin: EdgeInsets.zero,
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 100)),
                    child: FileUploadWidget(
                      width: 1.5,
                      icon: Icons.file_copy_outlined,
                      imageName: vaccineCertificate == null
                          ? null
                          : vaccineCertificate.path.split("/").last,
                      onPressed: () {
                        pickVaccineCertificate();
                      },
                    ),
                  ),
                ),
                Row(
                  children: [
                    AddressHeadingWidget(
                      title: "Update Your Temprature",
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                Container(
                  child: AddressBoxTextFeild(
                      icon: Icons.title,
                      title: "Temprature",
                      textEditingController: tempTextEditingController),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 11),
                ),
                RestaurantButton(
                  onPressed: () {
                    if (tempTextEditingController != null ||
                        vaccineCertificate != null) {}
                  },
                  title: "Update",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
