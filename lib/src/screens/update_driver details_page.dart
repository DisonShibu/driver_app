import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_box_text_feild.dart';
import 'package:app_template/src/widgets/build_dropdown_categories.dart';
import 'package:app_template/src/widgets/description_textfield.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class UpdateDriverDetailsPage extends StatefulWidget {
  @override
  _UpdateDriverDetailsPageState createState() =>
      _UpdateDriverDetailsPageState();
}

class _UpdateDriverDetailsPageState extends State<UpdateDriverDetailsPage> {
  TextEditingController nameTextEditingController = new TextEditingController();
  TextEditingController phoneTextEditingController =
      new TextEditingController();
  TextEditingController emailTextEditingController =
      new TextEditingController();

  List<String> categories = [
    "+971",
    "+966",
    "+974",
    "+968",
  ];
  String values = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Constants.kitGradients[0],
      body: Stack(
        children: [
          Column(
            children: [
              Container(
                height: screenHeight(context, dividedBy: 2),
                color: Constants.kitGradients[2],
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                    Image.asset(
                      "assets/images/delivery_app_icon.jpg",
                      height: screenHeight(context, dividedBy: 7),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 4),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 40)),
                child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 1.7),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[3]
                            .withOpacity(0.9)
                            .withOpacity(0.9),
                        borderRadius: BorderRadius.circular(30)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: screenHeight(context, dividedBy: 30),
                          horizontal: screenWidth(context, dividedBy: 17)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Phone Number",
                                style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 19,
                                  fontFamily: "OswaldRegular",
                                ),
                              ),
                            ],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                height: screenHeight(context, dividedBy: 16),
                                decoration: BoxDecoration(
                                    color: Constants.kitGradients[0],
                                    borderRadius: BorderRadius.circular(20),
                                    border: Border.all(
                                        color: Constants.kitGradients[0],
                                        width: 0.0)),
                                child: Center(
                                  child: Center(
                                    child: DropDownFormField(
                                      hintText: true,
                                      dropDownList: categories,
                                      fontFamily: "OpenSansRegular",
                                      textBoxWidth: 6.3,
                                      boxHeight: 17,
                                      boxWidth: 6.2,
                                      dropDownValue: values,
                                      hintFontFamily: "OpenSansRegular",
                                      underLineFalse: false,
                                      centreAlignText: true,
                                      onClicked: (value) {
                                        setState(() {
                                          values = value;
                                        });
                                      },
                                      hintPadding: 30,
                                      title: "+971",
                                    ),
                                  ),
                                ),
                              ),
                              Spacer(
                                flex: 1,
                              ),
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  width: screenWidth(context, dividedBy: 1.6),
                                  child: DescriptionTextField(
                                    hintText: "PhoneNumber",
                                    textEditingController:
                                        phoneTextEditingController,
                                    suffixIcon: Icon(
                                      Icons.phone_android,
                                      size: 30,
                                      color: Colors.green,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Row(
                            children: [
                              Text(
                                "UserName",
                                style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 19,
                                  fontFamily: "OswaldRegular",
                                ),
                              ),
                            ],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          DescriptionTextField(
                            hintText: "UserName",
                            phoneNumber: true,
                            suffixIcon: Icon(
                              Icons.person_outline,
                              color: Colors.green,
                              size: 30,
                            ),
                            textEditingController: phoneTextEditingController,
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          Row(
                            children: [
                              Text(
                                "Email",
                                style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontWeight: FontWeight.w400,
                                  fontSize: 19,
                                  fontFamily: "OswaldRegular",
                                ),
                              ),
                            ],
                          ),
                          Spacer(
                            flex: 1,
                          ),
                          DescriptionTextField(
                            hintText: "Email",
                            phoneNumber: true,
                            suffixIcon: Icon(
                              Icons.email_outlined,
                              color: Colors.green,
                              size: 30,
                            ),
                            textEditingController: phoneTextEditingController,
                          ),
                          Spacer(
                            flex: 6,
                          ),
                          Container(
                            child: RestaurantButton(
                              title: "Update Details",
                              onPressed: () {
                                push(context, HomePage());
                                if (emailTextEditingController.text != null ||
                                    phoneTextEditingController.text != null ||
                                    nameTextEditingController.text != null) {
                                } else {
                                  showToast("No data is Entered");
                                }
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
