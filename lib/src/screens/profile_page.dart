import 'package:app_template/src/screens/update_driver%20details_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/home_tab_bar.dart';
import 'package:app_template/src/widgets/personal_details_widget.dart';
import 'package:app_template/src/widgets/profile_icon_widget.dart';
import 'package:app_template/src/widgets/profile_page_information_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  TextEditingController phoneNumberTextEditingController =
      new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          backgroundColor: Constants.kitGradients[0],
          body: SingleChildScrollView(
            child: Column(children: [
              // AppBarDelivery(
              //   color: Constants.kitGradients[0],
              //   title: "",
              //   icon: Icons.arrow_back_ios,
              //   onPressedLeftIcon: () {},
              //   isWhite: false,
              // ),
              Container(
                height: screenHeight(context, dividedBy: 3),
                width: screenWidth(context, dividedBy: 1),
                color: Constants.kitGradients[2],
                child: Column(
                  children: [
                    Spacer(
                      flex: 1,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 20),
                        ),
                        GestureDetector(
                          onTap: () {
                            pop(context);
                          },
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 4),
                        ),
                        Text(
                          "PROFILE",
                          style: TextStyle(
                              fontSize: screenWidth(context, dividedBy: 20),
                              color: Constants.kitGradients[0],
                              fontWeight: FontWeight.w400,
                              fontFamily: "SofiaProRegular"),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                    Spacer(
                      flex: 1,
                    ),
                    ProfileIconWidget(
                      color: Constants.kitGradients[0],
                      phoneNumber: "7560886107",
                      userName: "Dison Shibu",
                    ),
                    Spacer(
                      flex: 3,
                    ),
                  ],
                ),
              ),
              Column(children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20)),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 70),
                      ),
                      Row(
                        children: [
                          AddressHeadingWidget(
                            title: "Personal Details",
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 70),
                      ),
                      PersonalDetails(
                        email: "JohnDoe2222015@gmail.com",
                        alternatePhoneNumber: "NA",
                        onPressedEdit: () {
                          reviewDialog(
                              contentPadding: 80,
                              context: context,
                              insetPadding: 3,
                              msg: "Alternate PhoneNumber",
                              reviewTexteditingController:
                                  phoneNumberTextEditingController,
                              text1: "Hello",
                              text2: "World",
                              onPressed: () {
                                setState(() {});
                              },
                              titlePadding: 0);
                        },
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Card(
                        child: Padding(
                          padding: EdgeInsets.all(8.0),
                          child: ProfilePageInformationTile(
                            icon: Icons.person_outline,
                            title: " Manage Profile",
                            onPressed: () {
                              push(context, UpdateDriverDetailsPage());
                            },
                          ),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      Card(
                        elevation: 3,
                        color: Constants.kitGradients[0],
                        child: Container(
                          height: screenHeight(context, dividedBy: 4),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal:
                                            screenWidth(context, dividedBy: 20),
                                        vertical: screenHeight(context,
                                            dividedBy: 50)),
                                    child: Text(
                                      "More",
                                      style: TextStyle(
                                        color: Constants.kitGradients[5],
                                        fontWeight: FontWeight.w400,
                                        fontSize: 23,
                                        fontFamily: "PrompLight",
                                      ),
                                    ),
                                  ),
                                ],
                                crossAxisAlignment: CrossAxisAlignment.center,
                              ),
                              ProfilePageInformationTile(
                                onPressed: () {},
                                title: "Help and support",
                                icon: Icons.headset_mic_outlined,
                                isArrow: false,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 80),
                              ),
                              ProfilePageInformationTile(
                                onPressed: () {},
                                title: "Terms",
                                icon: Icons.book,
                                isArrow: false,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 80),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ])
            ]),
          )),
    );
  }
}
