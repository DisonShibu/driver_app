import 'dart:io';

import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/screens/registration_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_heading_widget.dart';
import 'package:app_template/src/widgets/app_bar_delivery.dart';
import 'package:app_template/src/widgets/file_upload_text.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';

class UploadFilesPage extends StatefulWidget {
  @override
  _UploadFilesPageState createState() => _UploadFilesPageState();
}

class _UploadFilesPageState extends State<UploadFilesPage> {
  TextEditingController emailTextEditingController;
  TextEditingController nameTextEditingController;
  TextEditingController phoneNumberTextEditingController;
  File license;
  File idProof;
  bool isLicense = false;

  Future<void> pickLicense() async {
    FilePickerResult result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['jpg', 'pdf', 'doc'],
    );

    if (result != null) {
      File fileName = File(result.files.single.path);

      if (isLicense == true) {
        license = fileName;
      } else {
        idProof = fileName;
      }
      setState(() {});
    } else {
      // User canceled the picker
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 13)),
        child: AppBarDelivery(
          title: "RegistrationPage",
          elevation: 3,
          onPressedLeftIcon: () {
            pop(context);
          },
          color: Constants.kitGradients[0],
          leftIcon: Icons.arrow_back_ios,
          leftIconTrue: true,
          isWhite: true,
        ),
      ),
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 10)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 2.6),
                  height: screenHeight(context, dividedBy: 5),
                  child: Image.asset(
                    "assets/images/delivery_app_icon.jpg",
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                Row(
                  children: [
                    Container(
                        width: screenWidth(context, dividedBy: 1.7),
                        child: FittedBox(
                            child: Text(
                          "Welcome to flutter resturant !",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 30,
                              fontFamily: 'PromptLight'),
                        ))),
                  ],
                ),
                Row(
                  children: [
                    AddressHeadingWidget(
                      title: "Upload License",
                    ),
                  ],
                ),
                FileUploadWidget(
                  width: 1.5,
                  icon: Icons.file_copy_outlined,
                  imageName:
                      license == null ? null : license.path.split("/").last,
                  onPressed: () {
                    isLicense = true;
                    pickLicense();
                  },
                ),
                Row(
                  children: [
                    AddressHeadingWidget(
                      title: "Upload IdProof",
                    ),
                  ],
                ),
                FileUploadWidget(
                  width: 1.5,
                  icon: Icons.file_copy_outlined,
                  imageName:
                      idProof == null ? null : idProof.path.split("/").last,
                  onPressed: () {
                    isLicense = false;
                    pickLicense();
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 11),
                ),
                RestaurantButton(
                  onPressed: () {
                    pushAndRemoveUntil(context, HomePage(), false);
                  },
                  title: "REGISTER",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
