import 'dart:io';
import 'package:app_template/src/screens/home_page.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/login_text_box.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailTextEditingController;
  TextEditingController passwordTextEditingController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 10)),
            child: Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 10),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 2.6),
                  height: screenHeight(context, dividedBy: 5),
                  child: Image.asset(
                    "assets/images/delivery_app_icon.jpg",
                    fit: BoxFit.fill,
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Row(
                  children: [
                    Container(
                        width: screenWidth(context, dividedBy: 1.7),
                        child: FittedBox(
                            child: Text(
                          "Welcome to flutter resturant !",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 30,
                              fontFamily: 'PromptLight'),
                        ))),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      LoginTextBox(
                        phoneNumber: true,
                        controller: emailTextEditingController,
                        icon: Icons.email_outlined,
                        hintText: "Email",
                      ),

                      SizedBox(
                        height: screenHeight(context, dividedBy: 50),
                      ),

                      LoginTextBox(
                        phoneNumber: true,
                        controller: passwordTextEditingController,
                        hintText: "PhoneNumber",
                        password: true,
                      ),

                      // Divider(
                      //   color: Colors.black54,
                      // ),
                      // LoginTextBox(
                      //   controller: passwordTextEditingController,
                      //   icon: Icons.lock,
                      //   hintText: "Otp",
                      // ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                RestaurantButton(
                  onPressed: () {
                    pushAndReplacement(context, HomePage());
                  },
                  title: "LOGIN",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
