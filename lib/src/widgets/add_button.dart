import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AddButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  final String icon;
  AddButton({this.title, this.onPressed, this.icon});

  @override
  _AddButtonState createState() => _AddButtonState();
}

class _AddButtonState extends State<AddButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 23),
      child: ElevatedButton(
        onPressed: () {
          widget.onPressed();
        },
        style: ElevatedButton.styleFrom(
          primary: Constants.kitGradients[0],
          onPrimary: Constants.kitGradients[0],
          elevation: 3,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(7),
              side: BorderSide(color: Constants.kitGradients[2])),
        ),
        child: Text(
          widget.title,
          style: TextStyle(
            color: Constants.kitGradients[5],
            fontFamily: 'PromptLight',
          ),
        ),
      ),
    );
  }
}
