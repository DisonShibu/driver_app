import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class LocationBox extends StatefulWidget {
  final ValueChanged onLocationChanged;
  final Function onPressed;
  final String heading;
  final String subHeading;
  final IconData icon;
  final double padding;
  final bool dividerTrue;
  final bool subHeadingTrue;
  final double boxWidth;
  final bool colorWhite;
  LocationBox(
      {this.onLocationChanged,
      this.heading,
      this.subHeading,
      this.icon,
      this.onPressed,
      this.dividerTrue,
      this.subHeadingTrue,
      this.boxWidth,
      this.colorWhite,
      this.padding});
  @override
  _LocationBoxState createState() => _LocationBoxState();
}

class _LocationBoxState extends State<LocationBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: widget.padding),
        ),
        child: Column(
          children: [
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    widget.icon,
                    size: 30,
                    color: widget.colorWhite == true
                        ? Colors.white
                        : Colors.black26,
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  Column(
                    children: [
                      Container(
                        width: screenWidth(context, dividedBy: widget.boxWidth),
                        child: Text(
                          widget.heading,
                          style: TextStyle(
                              color: widget.colorWhite == true
                                  ? Constants.kitGradients[0]
                                  : Constants.kitGradients[5],
                              fontFamily: "PrompLight",
                              fontSize: screenWidth(context, dividedBy: 20)),
                        ),
                      ),
                      widget.subHeadingTrue == true
                          ? Container(
                              width: screenWidth(context,
                                  dividedBy: widget.boxWidth),
                              child: Text(
                                widget.subHeading,
                                style: TextStyle(
                                    color: Constants.kitGradients[4],
                                    fontFamily: 'OpenSansRegular',
                                    fontSize:
                                        screenWidth(context, dividedBy: 26)),
                              ),
                            )
                          : Container()
                    ],
                  )
                ],
              ),
            ),
            widget.dividerTrue == true
                ? Divider(
                    color: Colors.black26,
                    thickness: 1.0,
                  )
                : SizedBox(
                    width: 1,
                  )
          ],
        ),
      ),
    );
  }
}
