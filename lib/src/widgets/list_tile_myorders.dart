import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// ignore: must_be_immutable
class ListTileMyOrders extends StatefulWidget {
  final String foodImage;
  final String name;
  final String orderNum;
  final String address;
  final bool orderNumber;
  final bool isPrice;
  final double doubleWidth;
  final String price;
  final double doubleHeight;
  ListTileMyOrders(
      {this.foodImage,
      this.name,
      this.orderNum,
      this.isPrice,
      this.address,
      this.price,
      this.doubleWidth,
      this.doubleHeight,
      this.orderNumber});
  @override
  _ListTileMyOrdersState createState() => _ListTileMyOrdersState();
}

class _ListTileMyOrdersState extends State<ListTileMyOrders> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 40)),
      child: Card(
        color: Constants.kitGradients[0],
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8),
        ),
        child: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 7),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              widget.doubleWidth != null
                  ? SizedBox(
                      width: screenWidth(context, dividedBy: 40),
                    )
                  : SizedBox(),
              Container(
                width: screenWidth(context,
                    dividedBy:
                        widget.doubleWidth == null ? 3.2 : widget.doubleWidth),
                height: screenHeight(context,
                    dividedBy:
                        widget.doubleHeight == null ? 7 : widget.doubleHeight),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8),
                  child: Image.network(
                    widget.foodImage,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              widget.doubleWidth != null
                  ? SizedBox(
                      width: screenWidth(context, dividedBy: 40),
                    )
                  : SizedBox(),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 50)),
                child: Container(
                  height: screenHeight(context, dividedBy: 7),
                  width: screenWidth(context, dividedBy: 2.4),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        widget.name,
                        style: TextStyle(
                            color: Constants.kitGradients[3],
                            fontFamily: "PrompLight",
                            fontSize: screenWidth(context, dividedBy: 20)),
                      ),
                      widget.orderNumber != false
                          ? Text(
                              widget.orderNum,
                              style: TextStyle(
                                  color: Constants.kitGradients[3],
                                  fontFamily: "PrompLight",
                                  fontSize:
                                      screenWidth(context, dividedBy: 35)),
                            )
                          : Container(),
                      Text(
                        widget.address,
                        style: TextStyle(
                            color: Constants.kitGradients[3],
                            fontFamily: "PrompLight",
                            fontSize: screenWidth(context, dividedBy: 35)),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    widget.isPrice != false
                        ? Text(
                            "Price",
                            style: TextStyle(
                                color: Constants.kitGradients[3],
                                fontFamily: "PrompLight",
                                fontSize: screenWidth(context, dividedBy: 26)),
                          )
                        : Container(),
                    Text(
                      widget.price,
                      style: TextStyle(
                          color: Constants.kitGradients[3],
                          fontFamily: "PrompLight",
                          fontSize: screenWidth(context, dividedBy: 29)),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
