import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabBarTile extends StatefulWidget {
  final String title;
  final bool tileSelected;
  final Function onPressed;
  TabBarTile({this.title, this.tileSelected, this.onPressed});
  @override
  _TabBarTileState createState() => _TabBarTileState();
}

class _TabBarTileState extends State<TabBarTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        color: Colors.transparent,
        child: Column(
          children: [
            Spacer(
              flex: 1,
            ),
            Text(
              widget.title,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontFamily: 'PrompLight',
                  fontWeight: FontWeight.w600,
                  letterSpacing: 2),
            ),
            Spacer(
              flex: 1,
            ),
            Container(
              height: screenHeight(context, dividedBy: 170),
              width: screenWidth(context, dividedBy: 2.2),
              decoration: BoxDecoration(
                color: widget.tileSelected == true
                    ? Constants.kitGradients[2]
                    : Constants.kitGradients[0],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
