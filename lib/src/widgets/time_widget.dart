import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TimeWidget extends StatefulWidget {
  final String heading;
  final String time;
  TimeWidget({this.time, this.heading});

  @override
  _TimeWidgetState createState() => _TimeWidgetState();
}

class _TimeWidgetState extends State<TimeWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 2.8),
      height: screenHeight(context, dividedBy: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Text(
              widget.heading,
              style: TextStyle(
                  color: Constants.kitGradients[5],
                  fontFamily: "PrompLight",
                  fontSize: screenWidth(context, dividedBy: 26)),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Icon(
                Icons.access_alarm_outlined,
                color: Constants.kitGradients[2],
                size: 20,
              ),
              SizedBox(
                width: 6,
              ),
              Text(
                widget.time,
                style: TextStyle(
                    color: Constants.kitGradients[5],
                    fontFamily: "OpenSansRegular",
                    fontSize: screenWidth(context, dividedBy: 23)),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
