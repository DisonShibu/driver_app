import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginTextBox extends StatefulWidget {
  final IconData icon;
  final TextEditingController controller;
  final String hintText;
  final bool phoneNumber;
  final bool password;
  LoginTextBox(
      {this.controller,
      this.icon,
      this.hintText,
      this.phoneNumber,
      this.password});

  @override
  _LoginTextBoxState createState() => _LoginTextBoxState();
}

class _LoginTextBoxState extends State<LoginTextBox> {
  bool obscureTextTrue = false;
  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: obscureTextTrue,
      keyboardType: widget.phoneNumber == true
          ? TextInputType.number
          : TextInputType.text,
      controller: widget.controller,
      style: TextStyle(color: Constants.kitGradients[3]),
      decoration: InputDecoration(
          prefixIcon: GestureDetector(
              onTap: () {
                if (widget.password == true) {
                  setState(() {
                    obscureTextTrue = !obscureTextTrue;
                  });
                }
              },
              child: Icon(
                widget.password != true
                    ? widget.icon
                    : obscureTextTrue == false
                        ? Icons.visibility_off_outlined
                        : Icons.visibility_outlined,
                size: 20,
                color: Constants.kitGradients[3],
              )),
          contentPadding: const EdgeInsets.symmetric(vertical: 0),
          focusedBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Constants.kitGradients[3], width: 2.0),
            borderRadius: BorderRadius.circular(25.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: Constants.kitGradients[3], width: 2.0),
            borderRadius: BorderRadius.circular(25.0),
          ),
          hintText: widget.hintText,
          hintStyle: TextStyle(color: Constants.kitGradients[3]),
          border: InputBorder.none),
    );
  }
}
