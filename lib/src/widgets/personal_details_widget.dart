import 'package:app_template/src/widgets/label_value_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PersonalDetails extends StatefulWidget {
  final String email;
  final String alternatePhoneNumber;
  final Function onPressedEdit;
  PersonalDetails({this.alternatePhoneNumber, this.email, this.onPressedEdit});
  @override
  _PersonalDetailsState createState() => _PersonalDetailsState();
}

class _PersonalDetailsState extends State<PersonalDetails> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        LabelAndValueWidget(
          label: "Email",
          value: widget.email,
          width2: 1.8,
          width1: 3,
        ),
        LabelAndValueWidget(
          label: "Alternate Phone",
          value: widget.alternatePhoneNumber,
          editTrue: true,
          width1: 3,
          width2: 2,
          onTapEdit: () {
            widget.onPressedEdit();
          },
        )
      ],
    );
  }
}
