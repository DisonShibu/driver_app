import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryWidget extends StatefulWidget {
  final String images;
  final int currentIndex;
  final String title;
  final Function onPressed;
  CategoryWidget({this.images, this.currentIndex, this.onPressed, this.title});

  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<CategoryWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Container(
        height: screenHeight(context, dividedBy: 5),
        width: screenWidth(context, dividedBy: 2),
        decoration: BoxDecoration(
            color: Colors.black, borderRadius: BorderRadius.circular(20)),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: Container(
                height: screenHeight(context, dividedBy: 4.1),
                width: screenWidth(context, dividedBy: 2),
                child: Image.asset(
                  widget.images,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Column(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 20),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 20),
                    ),
                    Container(
                        width: screenWidth(context, dividedBy: 4),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              color: Constants.kitGradients[5],
                              fontSize: 20,
                              fontFamily: "OswaldRegular",
                              fontWeight: FontWeight.w800),
                        )),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
