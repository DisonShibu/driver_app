import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

// ignore: must_be_immutable
class DeliveryDistanceWidget extends StatefulWidget {
  final IconData icon;
  final String title;
  final bool iconBool;
  final String subHeading;
  final String thirdLine;
  final bool orderNumber;
  final double doubleWidth;
  final double doubleHeight;
  final String price;
  DeliveryDistanceWidget(
      {this.icon,
      this.title,
      this.subHeading,
      this.thirdLine,
      this.iconBool,
      this.price,
      this.doubleHeight,
      this.doubleWidth,
      this.orderNumber});
  @override
  _DeliveryDistanceWidgetState createState() => _DeliveryDistanceWidgetState();
}

class _DeliveryDistanceWidgetState extends State<DeliveryDistanceWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 40)),
      child: Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: widget.doubleHeight),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            widget.iconBool == true
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          width: screenWidth(context, dividedBy: 8),
                          height: screenHeight(context, dividedBy: 18),
                          decoration: BoxDecoration(
                              color: Constants.kitGradients[1].withOpacity(0.3),
                              shape: BoxShape.circle),
                          child: Center(
                            child: Icon(
                              widget.icon,
                              size: screenWidth(context, dividedBy: 14),
                            ),
                          )),
                    ],
                  )
                : Container(),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 50)),
              child: Container(
                height: screenHeight(context, dividedBy: widget.doubleHeight),
                width: screenWidth(context, dividedBy: widget.doubleWidth),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      widget.title,
                      style: TextStyle(
                          color: Constants.kitGradients[5],
                          fontFamily: "PrompLight",
                          fontSize: screenWidth(context, dividedBy: 20)),
                    ),
                    widget.orderNumber != false
                        ? Text(
                            widget.subHeading,
                            style: TextStyle(
                                color: Constants.kitGradients[5],
                                fontFamily: "PrompLight",
                                fontSize: screenWidth(context, dividedBy: 35)),
                          )
                        : Container(),
                    Text(
                      widget.thirdLine,
                      style: TextStyle(
                          color: Constants.kitGradients[2],
                          fontFamily: "PrompLight",
                          fontSize: screenWidth(context, dividedBy: 30)),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // widget.isPrice != false
                  //     ? Text(
                  //         "SR",
                  //         style: TextStyle(
                  //             color: Constants.kitGradients[3],
                  //             fontFamily: "PrompLight",
                  //             fontSize: screenWidth(context, dividedBy: 26)),
                  //       )
                  //     : Container(),
                  Text(
                    widget.price + " SR",
                    style: TextStyle(
                        color: Constants.kitGradients[3],
                        fontFamily: "PrompLight",
                        fontSize: screenWidth(context, dividedBy: 29)),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
