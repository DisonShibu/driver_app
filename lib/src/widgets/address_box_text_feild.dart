import 'dart:ui';

import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddressBoxTextFeild extends StatefulWidget {
  final String title;
  final IconData icon;
  final bool phoneNumberTrue;
  final TextEditingController textEditingController;
  AddressBoxTextFeild(
      {this.textEditingController,
      this.title,
      this.icon,
      this.phoneNumberTrue});

  @override
  _AddressBoxTextFeildState createState() => _AddressBoxTextFeildState();
}

class _AddressBoxTextFeildState extends State<AddressBoxTextFeild> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      cursorColor: Colors.black45,
      keyboardType: widget.phoneNumberTrue == true
          ? TextInputType.number
          : TextInputType.text,
      controller: widget.textEditingController,
      selectionHeightStyle: BoxHeightStyle.tight,
      style: TextStyle(color: Colors.black, fontFamily: "OpenSansRegular"),
      decoration: InputDecoration(
        fillColor: Colors.white,
        contentPadding: EdgeInsets.only(top: 14),
        prefixIcon: Icon(
          widget.icon,
          size: 30,
          color: Colors.black26,
        ),

        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.black26),
        ),
        // enabledBorder: OutlineInputBorder(
        //   borderSide: BorderSide(
        //       color: Colors.transparent.withOpacity(0.1), width: 0),
        //   borderRadius: BorderRadius.circular(10),
        // ),
        hintText: widget.title,
        hintStyle: TextStyle(color: Colors.black87, fontFamily: "PrompLight"),
      ),
    );
  }
}
