import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/address_box_text_feild.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:flutter/material.dart';

class WriteReviewAlertBox extends StatefulWidget {
  final String title;
  final double insetPadding;
  final double titlePadding;
  final double contentPadding;
  final Function onPressed;
  final TextEditingController reviewTextEditingController;
  WriteReviewAlertBox(
      {this.title,
      this.onPressed,
      this.contentPadding,
      this.insetPadding,
      this.titlePadding,
      this.reviewTextEditingController});
  @override
  _WriteReviewAlertBoxState createState() => _WriteReviewAlertBoxState();
}

class _WriteReviewAlertBoxState extends State<WriteReviewAlertBox> {
  FocusNode yourFocus = FocusNode();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(
          top: screenHeight(context,
              dividedBy: 3)), // adjust values according to your need),
      child: AlertDialog(
        // insetPadding: EdgeInsets.symmetric(
        //   vertical: screenHeight(context, dividedBy: widget.insetPadding),
        // ),
        titlePadding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: widget.titlePadding),
        ),
        contentPadding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: widget.contentPadding)),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(30),
          ),
        ),
        backgroundColor: Colors.white,
        content: Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 4),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 70),
                  ),
                  Text(
                    widget.title,
                    style: TextStyle(
                        fontFamily: "sfProSemiBold",
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 70),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 40)),
                child: Container(
                    height: screenHeight(context, dividedBy: 12),
                    child: AddressBoxTextFeild(
                      textEditingController:
                          widget.reviewTextEditingController,
                      phoneNumberTrue: true,
                      title: "Phone Number",
                      icon: Icons.phone_android,
                    )),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 70),
              ),
              RestaurantButton(
                title: "SUBMIT",
                onPressed: () {
                  widget.onPressed();
                  pop(context);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
