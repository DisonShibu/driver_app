import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class FileUploadWidget extends StatefulWidget {
  final IconData icon;
  final String imageName;
  final Function onPressed;
  final double width;
  FileUploadWidget({this.icon, this.imageName, this.onPressed, this.width});

  @override
  _FileUploadWidgetState createState() => _FileUploadWidgetState();
}

class _FileUploadWidgetState extends State<FileUploadWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        widget.onPressed();
      },
      child: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 30),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(
                widget.icon,
                color: Constants.kitGradients[6],
                size: 20,
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 20),
              ),
              Container(
                width: screenWidth(context, dividedBy: widget.width),
                child: Text(
                  widget.imageName == null
                      ? "No File Uploaded"
                      : widget.imageName,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontFamily: 'PromptLight'),
                ),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
        ],
      ),
    );
  }
}
