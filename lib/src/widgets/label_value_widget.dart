import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/material.dart';

class LabelAndValueWidget extends StatefulWidget {
  final String label;
  final String value;
  final double width1;
  final double width2;
  final bool iconTrue;
  final bool editTrue;
  final Function onTapEdit;
  LabelAndValueWidget({
    this.label,
    this.value,
    this.iconTrue,
    this.width1,
    this.width2,
    this.editTrue,
    this.onTapEdit,
  });

  @override
  _LabelAndValueWidgetState createState() => _LabelAndValueWidgetState();
}

class _LabelAndValueWidgetState extends State<LabelAndValueWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: screenWidth(context,
                dividedBy: widget.width1 == null ? 2.4 : widget.width1),
            alignment: Alignment.centerLeft,
            child: widget.iconTrue == true
                ? Image.asset(
                    widget.label,
                    fit: BoxFit.fitHeight,
                  )
                : Text(
                    widget.label,
                    style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                      fontFamily: "OswaldRe",
                    ),
                  ),
          ),
          widget.editTrue == true
              ? Container(
                  width: screenWidth(context,
                      dividedBy: widget.width1 == null ? 2.4 : widget.width2),
                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    onTap: () {
                      widget.onTapEdit();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text(
                          widget.value == null ? "NA" : widget.value,
                          style: TextStyle(
                            color: Constants.kitGradients[5],
                            fontWeight: FontWeight.w400,
                            fontSize: 18,
                            fontFamily: "OswaldRe",
                          ),
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 80),
                        ),
                        Icon(
                          Icons.edit,
                          size: 16,
                          color: Constants.kitGradients[2],
                        )
                      ],
                    ),
                  ),
                )
              : Container(
                  width: screenWidth(context,
                      dividedBy: widget.width1 == null ? 2.4 : widget.width2),
                  alignment: Alignment.centerRight,
                  child: Text(
                    widget.value == null ? "NA" : widget.value,
                    style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                      fontFamily: "OswaldRe",
                    ),
                  ),
                )
        ],
      ),
    );
  }
}
