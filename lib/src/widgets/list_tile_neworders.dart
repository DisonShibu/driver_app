import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:app_template/src/widgets/restuarant_button.dart';
import 'package:app_template/src/widgets/saved_address_widget.dart';
import 'package:app_template/src/widgets/time_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListTileNewOrders extends StatefulWidget {
  final String hotelAddress;
  final String hotelName;
  final String userName;
  final String userAddress;
  final Function onPressedViewDetails;
  final double elevation;
  final bool buttonTrue;
  ListTileNewOrders(
      {this.hotelAddress,
      this.hotelName,
      this.onPressedViewDetails,
      this.userAddress,
      this.elevation,
      this.buttonTrue,
      this.userName});

  @override
  _ListTileNewOrdersState createState() => _ListTileNewOrdersState();
}

class _ListTileNewOrdersState extends State<ListTileNewOrders> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: widget.elevation == null ? 5 : widget.elevation,
      margin:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 40)),
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 40)),
        child: Column(
          children: [
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Row(
              children: [
                Text(
                  "Store Address",
                  style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontFamily: "PrompLight",
                      fontSize: 16),
                ),
              ],
            ),
            Container(
              height: screenHeight(context, dividedBy: 6),
              child: SavedAddressBox(
                center: false,
                svgIconTrue: false,
                icon: Icons.location_on_outlined,
                heading: widget.hotelName,
                subHeading: widget.hotelAddress,
                thirdLineTrue: false,
                onPressedAddress: () {},
              ),
            ),
            Row(
              children: [
                Text(
                  "Delivery Address",
                  style: TextStyle(
                      color: Constants.kitGradients[5],
                      fontFamily: "PrompLight",
                      fontSize: 16),
                ),
              ],
            ),
            Container(
              height: screenHeight(context, dividedBy: 5),
              child: SavedAddressBox(
                center: false,
                svgIconTrue: false,
                icon: Icons.location_on_outlined,
                image: "assets/images/userprofile.png",
                heading: widget.userName,
                subHeading: widget.userAddress,
                thirdLineTrue: true,
                thirdLine: "ph:76876878978",
                onPressedAddress: () {},
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TimeWidget(
                  heading: "PickUpTime",
                  time: "45 min",
                ),
                TimeWidget(
                  heading: "DeliveryTime",
                  time: " 1hr45 min",
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            widget.buttonTrue == true
                ? RestaurantButton(
                    title: "View Details",
                    onPressed: () {
                      widget.onPressedViewDetails();
                    },
                  )
                : Container(),
            widget.buttonTrue != true
                ? Container()
                : SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  )
          ],
        ),
      ),
    );
  }
}
