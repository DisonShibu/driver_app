import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RestaurantButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  final bool disabled;
  RestaurantButton({this.title, this.onPressed, this.disabled});

  @override
  _RestaurantButtonState createState() => _RestaurantButtonState();
}

class _RestaurantButtonState extends State<RestaurantButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 2),
      height: screenHeight(context, dividedBy: 20),
      decoration: BoxDecoration(
        color: Colors.transparent,
      ),
      child: ElevatedButton(
        onPressed: () {
          if (widget.disabled != true) widget.onPressed();
        },
        style: ElevatedButton.styleFrom(
          primary: widget.disabled == true
              ? Constants.kitGradients[6].withOpacity(0.3)
              : Constants.kitGradients[6],
          onPrimary: Constants.kitGradients[6],
          elevation: 9,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18),
          ),
        ),
        child: Text(
          widget.title,
          style: TextStyle(
              color: Constants.kitGradients[0],
              fontFamily: 'PromptLight',
              fontSize: 16),
        ),
      ),
    );
  }
}
