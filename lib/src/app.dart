import 'dart:async';
import 'dart:io';
import 'package:app_template/localisation/demo_local.dart';
import 'package:app_template/src/screens/login_page.dart';
import 'package:app_template/src/utils/constants.dart';
import 'package:app_template/src/utils/utils.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:app_template/src/utils/hive.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';

// const fetchBackground = "fetchBackground";
//
// void callbackDispatcher() {
//   Workmanager().executeTask((task, inputData) async {
//     switch (task) {
//       case fetchBackground:
//         _getUserAddress();
//         break;
//     }
//     return Future.value(true);
//   });
// }

Future<bool> checkPermissions() async {
  bool serviceEnabled;
  LocationPermission permission;

  // Test if location services are enabled.
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (serviceEnabled == true) {
    return serviceEnabled;
  } else {
    showToast("Please Enable Your Location Service");

    Future.error("Location Service Disabled");
  }

  permission = await Geolocator.checkPermission();

  if (permission == LocationPermission.whileInUse ||
      permission == LocationPermission.always) {
    return true;
  } else if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return Future.error("");
    }
  } else if (permission == LocationPermission.deniedForever) {
    if (Platform.isAndroid) {
      await Geolocator.openLocationSettings();
    } else {
      print("Ios");
    }
    return Future.error("Location Service Are Denied");
    // Permissions are denied forever, handle appropriately.

  } else {
    return true;
  }
}

// void _getUserAddress() async {
//   print("GET USER METHOD RUNNING =========");
//   checkPermissions().then((bool) async {
//     print("Location Fetched");
//     positionStream =
//         Geolocator.getPositionStream(intervalDuration: Duration(seconds: 3))
//             .listen((Position position) {
//       print(position == null
//           ? 'Unknown'
//           : "loc" +
//               position.latitude.toString() +
//               ', ' +
//               "loc" +
//               position.longitude.toString());
//     });
//   }).onError((error, stackTrace) {
//     print("Error" + error.toString());
//   });
// }

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(locale);
    // MyApp.setLocale(context, locality);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

String lc;
String cc;
Locale _locale;
Locale locality;
StreamSubscription<Position> positionStream;

class _MyAppState extends State<MyApp> {
  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  void initState() {
    setState(() {
      lc = AppHive().getLanguage();
      cc = AppHive().getLanguage();
    });
    // Workmanager().initialize(
    //   callbackDispatcher,
    //   isInDebugMode: true,
    // );
    // Workmanager().registerPeriodicTask(
    //   "1",
    //   "fetchBackground",
    //   frequency: Duration(seconds: 60),
    // );

    // directionsApi();

    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      // _locale = Locale(lc, cc) ?? Locale('en', 'US');
      if ((lc != null) && (cc != null)) {
        _locale = Locale(lc);
      } else {
        _locale = Locale('us');
      }
    });
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
          textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'Nexa',
              bodyColor: Constants.kitGradients[0],
              displayColor: Constants.kitGradients[0]),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        locale: _locale,
        localizationsDelegates: [
          DemoLocalizations
              .delegate, // ... app-specific localization delegate[s]
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocales) {
          for (var locale in supportedLocales) {
            if (locale.languageCode == deviceLocale.languageCode &&
                locale.countryCode == deviceLocale.countryCode) {
              return deviceLocale;
            }
          }
          return supportedLocales.first;
        },
        supportedLocales: [
          Locale('en'), // English
          Locale('ar'), //Arabic
        ],
        home: LoginPage());
  }

  Future<bool> checkPermissions() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (serviceEnabled == true) {
      return serviceEnabled;
    } else {
      showToast("Please Enable Your Location Service");

      Future.error("Location Service Disabled");
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.whileInUse ||
        permission == LocationPermission.always) {
      return true;
    } else if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error("");
      }
    } else if (permission == LocationPermission.deniedForever) {
      if (Platform.isAndroid) {
        await Geolocator.openLocationSettings();
      } else {
        print("Ios");
      }
      return Future.error("Location Service Are Denied");
      // Permissions are denied forever, handle appropriately.

    } else {
      return true;
    }
  }

  void _getUserAddress() async {
    print("GET USER METHOD RUNNING =========");
    checkPermissions().then((bool) async {
      print("Location Fetched");
      positionStream =
          Geolocator.getPositionStream(intervalDuration: Duration(seconds: 3))
              .listen((Position position) {
        print(position == null
            ? 'Unknown'
            : "loc" +
                position.latitude.toString() +
                ', ' +
                "loc" +
                position.longitude.toString());
      });
    }).onError((error, stackTrace) {
      print("Error" + error.toString());
    });
  }
}
